# Wwise Rust bindings

Wwise is a famous audio engine, written in cpp.

You can find here a simple lib that simply includes
rust's bindings made via the 'bindgen' crate, and a basic
example.

The Wwise folder contains its SDK, precompiled libs and include,
which are free to public via an executable 'Wwise Launcher', found
on Audiokinetic's site.

## Very Quick Start

`
cargo run --example music_event
`
