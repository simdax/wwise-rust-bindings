#include <AK/SoundEngine/Common/AkTypes.h>
#include <AK/SoundEngine/Common/AkModule.h>  
#include <AK/SoundEngine/Common/AkSoundEngine.h>
#include <AK/SoundEngine/Common/AkMemoryMgr.h>
#include <AK/SoundEngine/Common/IAkStreamMgr.h>  
#include <AK/MusicEngine/Common/AkMusicEngine.h>  
#include <AK/SpatialAudio/Common/AkSpatialAudio.h>  
#include <AK/Comm/AkCommunication.h>
#include <AK/Tools/Common/AkPlatformFuncs.h>

#include <AkFileLocationBase.h>
#include <AkFilePackageLowLevelIOBlocking.h>
#include <AkFileHelpers.h>

#include <Wwise_IDs.h>

#include <AkSoundEngineDLL.h>