extern crate bindgen;
extern crate cc;

use std::env;
use std::path::PathBuf;

const ROOT: &str = "wwise";

fn link_static() {
    let path = format!("{}\\SDK\\x64_vc160\\Profile\\lib", ROOT);
    let libs = vec![
        "AkSoundEngine",
        "AkMemoryMgr",
        "AkStreamMgr",
        "AkSpatialAudio",
        "AkMusicEngine",
        "CommunicationCentral",
        "user32",
        "AkVorbisDecoder",
        "AkMeterFX",
    ];

    println!("cargo:rustc-link-search={}", path);
    for lib in libs {
        println!("cargo:rustc-link-lib={}", lib);
    }
}

fn build_additional() {
    cc::Build::new()
        .cpp(true)
        .define("UNICODE", "1")
        .include(format!("{}\\SDK\\include", ROOT))
        .include(format!("{}\\SDK\\samples\\SoundEngine\\Win32", ROOT))
        .file(format!(
            "{}\\SDK\\samples\\SoundEngine\\Win32\\AkDefaultIOHookBlocking.cpp",
            ROOT
        ))
        .file(format!(
            "{}\\SDK\\samples\\SoundEngine\\Common\\AkMultipleFileLocation.cpp",
            ROOT
        ))
        .compile("FileIO");
    cc::Build::new()
        .cpp(true)
        .define("UNICODE", "1")
        .include(format!("{}\\SDK\\include", ROOT))
        .include(format!("{}\\SDK\\samples\\SoundEngine\\Win32", ROOT))
        .include(format!(
            "{}\\SDK\\samples\\DynamicLibraries\\AkSoundEngineDLL\\Win32",
            ROOT
        ))
        .file(format!(
            "{}\\SDK\\samples\\DynamicLibraries\\AkSoundEngineDLL\\AkSoundEngineDLL.cpp",
            ROOT
        ))
        .compile("soundengineDLL")
}

fn namespaces(builder: bindgen::Builder) -> bindgen::Builder {
    let mut b = builder;
    let api = vec![
        ("MemoryMgr", vec!["GetDefaultSettings", "Init"]),
        (
            "StreamMgr",
            vec![
                "GetDefaultSettings",
                "GetDefaultDeviceSettings",
                "Create",
                "SetCurrentLanguage",
            ],
        ),
        (
            "SoundEngine",
            vec![
                "GetDefaultInitSettings",
                "GetDefaultPlatformInitSettings",
                "Init",
                "LoadBank",
                "PostEvent",
                "RegisterGameObj",
                "SetDefaultListeners",
                "RenderAudio",
            ],
        ),
        (
            "SOUNDENGINE_DLL",
            vec!["Init", "Term", "Tick", "SetBasePath"],
        ),
        ("MusicEngine", vec!["Init", "GetDefaultInitSettings"]),
        ("SpatialAudio", vec!["Init"]),
        ("Comm", vec!["Init", "GetDefaultInitSettings"]),
    ];
    for (module, fns) in api {
        for f in fns {
            println!("AK::{}::{}", module, f);
            b = b.allowlist_function(format!("AK::{}::{}", module, f));
        }
    }
    b
}

fn make_bindings() -> bindgen::Bindings {
    let builder = bindgen::Builder::default()
        .clang_arg("-x")
        .clang_arg("c++")
        .clang_arg("-std=c++14")
        .clang_arg(format!("-I{}\\SDK\\include", ROOT))
        .clang_arg(format!("-I{}\\SDK\\samples\\SoundEngine\\Win32", ROOT))
        .clang_arg(format!("-I{}\\SDK\\samples\\SoundEngine\\Common", ROOT))
        .clang_arg(format!(
            "-I{}\\SDK\\samples\\DynamicLibraries\\AkSoundEngineDLL",
            ROOT
        ))
        .clang_arg(format!(
            "-I{}\\SDK\\samples\\IntegrationDemo\\WwiseProject\\GeneratedSoundBanks",
            ROOT
        ))
        .clang_arg("-DUNICODE")
        .header("wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .allowlist_type("CAkFilePackageLowLevelIOBlocking")
        .allowlist_type("CAkDefaultIOHookBlocking")
        .opaque_type("AkListBareLight");
    let builder = namespaces(builder);
    builder.generate().expect("Unable to generate bindings")
}

fn main() {
    println!("cargo:rerun-if-changed=wrapper.h");
    build_additional();
    link_static();
    make_bindings()
        .write_to_file(PathBuf::from(env::var("OUT_DIR").unwrap()).join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
