#[test]
fn init_test() {
    const ROOT: &str = r"C:\Program Files (x86)\Audiokinetic\Wwise 2021.1.2.7629";
    let root = format!(
        "{}\\SDK\\samples\\IntegrationDemo\\WwiseProject\\GeneratedSoundBanks\\Windows\\\0",
        ROOT
    );

    init(&root);
}

fn AKTEXT2(in_str: &str) -> *const u16 {
    in_str.encode_utf16().collect::<Vec<u16>>().as_ptr()
}

pub fn init(path: &str) {
    assert!(initMem(), "failed init Memory Mgr");
    assert!(initStreamMgr(), "failed init Stream Mgr");
    assert!(initStreamingDevice(path), "failed init Stream device");
    assert!(initSoundEngine(), "failed init Sound Engine");
    assert!(initMusicEngine(), "failed init Music Mgr");
    assert!(initSpatialAudio(), "failed init Spatial Audio");
    assert!(initComm(), "failed init Comm");
    unsafe {
        assert!(AK_StreamMgr_SetCurrentLanguage(AKTEXT2("English(US)\0")) == AKRESULT_AK_Success, "can't set lang");
    }
    assert!(
        load(vec![
            1355168291
        // "Init.bnk\0"
        ]),
        "failed load banks"
    );
}

// fn load(banks: Vec<&str>) -> bool {
fn load(banks: Vec<u32>) -> bool {
    unsafe {
        for bank in banks {
            // let mut id = std::mem::zeroed();
            AK_SoundEngine_LoadBank2(bank);
            // AK_SoundEngine_LoadBank(bank.encode_utf16().collect::<Vec<u16>>().as_ptr(), &mut id);
        }
    }
    true
}

fn initSoundEngine() -> bool {
    unsafe {
        let mut init_settings = std::mem::zeroed();
        let mut platform_settings = std::mem::zeroed();

        AK_SoundEngine_GetDefaultInitSettings(&mut init_settings);
        AK_SoundEngine_GetDefaultPlatformInitSettings(&mut platform_settings);
        AK_SoundEngine_Init(&mut init_settings, &mut platform_settings) == AKRESULT_AK_Success
    }
}

fn initSpatialAudio() -> bool {
    unsafe {
        let mut spatial_audio_settings = std::mem::zeroed();
        AK_SpatialAudio_Init(&mut spatial_audio_settings) == AKRESULT_AK_Success
    }
}

fn initMusicEngine() -> bool {
    unsafe {
        let mut musicSettings = std::mem::zeroed();
        AK_MusicEngine_GetDefaultInitSettings(&mut musicSettings);
        AK_MusicEngine_Init(&mut musicSettings) == AKRESULT_AK_Success
    }
}

fn initComm() -> bool {
    unsafe {
        let mut commSettings = std::mem::zeroed();
        AK_Comm_GetDefaultInitSettings(&mut commSettings);
        AK_Comm_Init(&mut commSettings) == AKRESULT_AK_Success
    }
}

fn initStreamMgr() -> bool {
    unsafe {
        let mut stmSettings = std::mem::zeroed();
        AK_StreamMgr_GetDefaultSettings(&mut stmSettings);
        AK_StreamMgr_Create(&mut stmSettings) != std::ptr::null_mut::<AK_IAkStreamMgr>()
    }
}

fn initStreamingDevice(path: &str) -> bool {
    unsafe {
        let mut g_lowLevelIO: CAkDefaultIOHookBlocking = std::mem::zeroed();
        let mut deviceSettings = std::mem::zeroed();

        AK_StreamMgr_GetDefaultDeviceSettings(&mut deviceSettings);
        assert!(
            CAkDefaultIOHookBlocking_Init(&mut g_lowLevelIO, &deviceSettings, true)
                == AKRESULT_AK_Success,
            "Can't init default io HOOK"
        );
        assert!(
            CAkMultipleFileLocation_AddBasePath(
                &mut g_lowLevelIO._base_2,
                std::ffi::OsString::from(path)
                    .into_string()
                    .expect(&format!("can't stringify{}", path))
                    .encode_utf16()
                    .collect::<Vec<u16>>()
                    .as_ptr()
            ) == AKRESULT_AK_Success
        );
        true
    }
}

fn initMem() -> bool {
    unsafe {
        let mut memSettings = std::mem::zeroed();
        AK_MemoryMgr_GetDefaultSettings(&mut memSettings);
        AK_MemoryMgr_Init(&mut memSettings) == AKRESULT_AK_Success
    }
}
