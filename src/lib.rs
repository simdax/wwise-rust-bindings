#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(deref_nullptr)]
#![allow(unaligned_references)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

pub fn aktext(in_str: &str) -> Vec<u16> {
    in_str.encode_utf16().chain(vec![0]).collect()
}

pub fn load_banks(banks: Vec<&str>) {
    for bank in banks {
        let mut id: AkBankID = 0;
        unsafe {
            assert!(
                AK_SoundEngine_LoadBank(aktext(bank).as_ptr(), &mut id) == AKRESULT_AK_Success,
                "pb with {}",
                bank
            );
        }
    }
}

pub fn event(name: &str) {
    unsafe {
        AK_SoundEngine_PostEvent1(
            aktext(name).as_ptr(),
            1,
            0,
            None,
            std::ptr::null_mut(),
            0,
            std::ptr::null_mut(),
            0,
        );
    }
}
