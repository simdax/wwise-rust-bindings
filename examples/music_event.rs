extern crate tokio;

use tokio::signal;
use wwise::*;

fn init() {
    const ROOT: &str = "wwise";
    let root = format!(
        "{}\\SDK\\samples\\IntegrationDemo\\WwiseProject\\GeneratedSoundBanks\\Windows\\\0",
        ROOT
    );

    unsafe {
        let mut mem_settings: AkMemSettings = std::mem::zeroed();
        let mut stream_settings: AkStreamMgrSettings = std::mem::zeroed();
        let mut device_settings: AkDeviceSettings = std::mem::zeroed();
        let mut init_settings: AkInitSettings = std::mem::zeroed();
        let mut platform_settings: AkPlatformInitSettings = std::mem::zeroed();
        let mut music_settings: AkMusicSettings = std::mem::zeroed();

        AK_MemoryMgr_GetDefaultSettings(&mut mem_settings);
        AK_StreamMgr_GetDefaultSettings(&mut stream_settings);
        AK_StreamMgr_GetDefaultDeviceSettings(&mut device_settings);
        AK_SoundEngine_GetDefaultInitSettings(&mut init_settings);
        AK_SoundEngine_GetDefaultPlatformInitSettings(&mut platform_settings);
        AK_MusicEngine_GetDefaultInitSettings(&mut music_settings);
        AK_SOUNDENGINE_DLL_Init(
            &mut mem_settings,
            &mut stream_settings,
            &mut device_settings,
            &mut init_settings,
            &mut platform_settings,
            &mut music_settings,
        );
        assert!(AK_SOUNDENGINE_DLL_SetBasePath(aktext(&root).as_ptr()) == AKRESULT_AK_Success);
        load_banks(vec!["Init.bnk", "MusicCallbacks.bnk"]);
        let listener: AkGameObjectID = 0;
        AK_SoundEngine_RegisterGameObj(listener);
        AK_SoundEngine_SetDefaultListeners(&listener, 1);
        AK_SoundEngine_RegisterGameObj(1);
    }
}

#[tokio::main]
async fn main() {
    init();
    event("PlayMusicDemo1");
    unsafe {
        AK_SOUNDENGINE_DLL_Tick();
        signal::ctrl_c().await.expect("failed to listen for event");
        AK_SOUNDENGINE_DLL_Term();
    }
}
